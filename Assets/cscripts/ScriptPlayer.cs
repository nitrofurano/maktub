﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptPlayer : MonoBehaviour
{
    // Start is called before the first frame update
    public float horizontalSpeed = 0f;
    public float verticalSpeed = 0f;
    public bool isTeleported = false;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow) && valid(Vector2.right))
        {
            Vector3 position = this.transform.position;
            float newX=position.x+ horizontalSpeed * Time.deltaTime;

            this.transform.position =new Vector3(newX,transform.position.y,transform.position.z);
        }
        if (Input.GetKey(KeyCode.LeftArrow) && valid(Vector2.left))
        {
            Vector3 position = this.transform.position;
            float newX = position.x - horizontalSpeed * Time.deltaTime;
            this.transform.position = new Vector3(newX, transform.position.y, transform.position.z);
        }
        if (Input.GetKey(KeyCode.UpArrow) && valid(Vector2.up))
        {
            Vector2 position = this.transform.position;
            float newY = position.y + verticalSpeed * Time.deltaTime;
            this.transform.position = new Vector3(transform.position.x,newY, transform.position.z);
        }
        if (Input.GetKey(KeyCode.DownArrow) && valid(Vector2.down))
            {
            Vector2 position = this.transform.position;
            position.y--;
            this.transform.position = position;
        }

    }

    bool valid(Vector3 dir)
    {
        Vector3 pos = transform.position;
        RaycastHit2D hit = Physics2D.Linecast(pos + dir, pos);
        return (hit.collider == GetComponent<Collider2D>());
    }
}
