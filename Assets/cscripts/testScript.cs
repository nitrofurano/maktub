﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class testScript : MonoBehaviour
{
    public Text CaixaDosPontos;
    public Text CaixaDoNivel;
    public GameObject OutroObjecto;
    public int MeusPontos;


    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.A))
        {
            IncrementaPontos(1);
        }
       
        MudaNivel();
    }

    public void IncrementaPontos(int soma)
    {
        int pontos = int.Parse(CaixaDosPontos.text);
        pontos += soma;
        MeusPontos =pontos;
        CaixaDosPontos.text = pontos.ToString();
    }

    public void MudaNivel()
    {
        CaixaDoNivel.text = "Nivel 55";
    }
}
