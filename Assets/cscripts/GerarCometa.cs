﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GerarCometa : MonoBehaviour
{
    public GameObject Cometa_prf;
    float randX;
    Vector2 whereToSpawn;

    public float spawnRate = 0f;
    float nextSpawn = 0.0f;
    // Use this for initialization 
    void Start() { }
    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            randX = Random.Range(-8.4f, 8.4f);
            whereToSpawn = new Vector2(randX, transform.position.y);
            Instantiate(Cometa_prf, whereToSpawn, Quaternion.identity);
        }
    }
}

//CometaSpawnerScript.cs
//How to spawn enemies randomly in Unity 2D game? (youtube)
//https://www.youtube.com/watch?v=AI8XNNRpTTw&t=5m56s
