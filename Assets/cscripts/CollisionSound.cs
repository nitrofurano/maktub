﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollisionSound : MonoBehaviour
{

    AudioSource source;


    // Use this for initialization
    void Start()
    {


        source = GetComponent<AudioSource>();

    }


    private void OnCollisionEnter(Collision collision)
    {
        source.Play();

    }
}