﻿using UnityEngine;
using System.Collections;
public class teleport : MonoBehaviour
{
    public float destinotemporariox;
    public float destinotemporarioy;
    public Transform Destination;       // Gameobject where they will be teleported to
    //public string TagList = "|teleport|"; // List of all tags that can teleport
                                          // Use this for initialization
    void Start() { }         // As needed
                             // Update is called once per frame
    void Update() { }         // As needed
    public void OnTriggerEnter2D(Collider2D other)
    {
        // If the tag of the colliding object is allowed to teleport
        if (other.name== "Maktub_Hero_rbcoll" && !other.GetComponent<ScriptPlayer>().isTeleported)
        {
            // Update other objects position and rotation
            
            //Destination.transform.position = new Vector3(destinotemporariox, destinotemporarioy);

            other.transform.position = Destination.transform.position;
            other.transform.rotation = Destination.transform.rotation;
            other.GetComponent<ScriptPlayer>().isTeleported = true;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "Maktub_Hero_rbcoll" && collision.GetComponent<ScriptPlayer>().isTeleported)
        {
            
            collision.GetComponent<ScriptPlayer>().isTeleported = false;
        }
    }
}
