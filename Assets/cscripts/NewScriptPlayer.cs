﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class NewScriptPlayer : MonoBehaviour {

public float speed = 1f;



// Start is called before the first frame update
void Start()
{
}

// Update is called once per frame
void Update()
{
    if (Input.GetKey(KeyCode.RightArrow) && valid(Vector2.right))
    {
        Vector3 position = this.transform.position;
        position.x += speed;
        this.transform.position = position;
    }
    if (Input.GetKey(KeyCode.LeftArrow) && valid(Vector2.left))
    {
        Vector3 position = this.transform.position;
        position.x -= speed;
        this.transform.position = position;
    }
    if (Input.GetKey(KeyCode.UpArrow) && valid(Vector2.up))
    {
        Vector2 position = this.transform.position;
        position.y += speed;
        this.transform.position = position;
    }
    if (Input.GetKey(KeyCode.DownArrow) && valid(Vector2.down))
    {
        Vector2 position = this.transform.position;
        position.y -= speed;
        this.transform.position = position;
    }

}

bool valid(Vector3 dir)
  {
    Vector3 pos = transform.position;
    RaycastHit2D hit = Physics2D.Linecast(pos + dir, pos);
   return (hit.collider == GetComponent<Collider2D>());
   }
}