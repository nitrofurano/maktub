﻿using UnityEngine;

public class Example : MonoBehaviour
{
    // Use GetComponent to access the animation
    void Start()
    {
        gameObject.GetComponent<Animation>().Play();
    }
}