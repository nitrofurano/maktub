﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExemploComunicacaoComOutrosScripts : MonoBehaviour
{
    //maneira 1 - crio uma referencia ao objecto que tem o script que eu quero e depois vou buscar
    //public GameObject OMeuAlvo;

    //maneira 2 - explicitamente dizer qual e o objecto que tem o script que nos queremos

    private void Update()
    {
        //maneira 1
        //Debug.Log("maneira 1 " + OMeuAlvo.GetComponent<testScript>().MeusPontos);

        //maneira 2 - me;lhor usar so uma vez em cada script quando ele faz load. nunca usar no update!!

        //Debug.Log("maneira 2 "+ GameObject.Find("starBW (6)").GetComponent<testScript>().MeusPontos);
    }
}
