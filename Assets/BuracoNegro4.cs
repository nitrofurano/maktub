﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuracoNegro4 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    { }

    public class Teleportation : MonoBehaviour
    {

        public GameObject BuracoNegro_prf4;
        public GameObject PlayerMaktub_Hero_rbcoll;


        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag == "Maktub_Hero_rbcoll")
            {
                StartCoroutine(Teleport());
            }
        }


        IEnumerator Teleport()
        {
            yield return new WaitForSeconds(0.5f);
            PlayerMaktub_Hero_rbcoll.transform.position = new Vector2(BuracoNegro_prf4.transform.position.x, BuracoNegro_prf4.transform.position.y);
        }
    }
}